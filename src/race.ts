import { ArrayInput } from "./types.js";

// you have to use the Promise object
export async function race(promises: ArrayInput): Promise<any> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}
//--------------------------------------------------
// same here - you have to use the Promise object
export async function some(promises: ArrayInput, num: number): Promise<any[]> {
    const results: any[] = [];

    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
