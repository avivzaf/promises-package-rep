import log from "@ajar/marker";
import { delay } from "./delay";

export const echo = async (msg: string, ms: number): Promise<string> => {
    await delay(ms);
    return msg;
};
