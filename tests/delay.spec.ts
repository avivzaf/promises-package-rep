import { delay } from "../src/delay";
import { expect } from "chai";

describe("delay tests", () => {
    context("type check", () => {
        it("check if type of 'delay' is function", async () => {
            expect(delay).to.be.a("function");
        });
    });
});
