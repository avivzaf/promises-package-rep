import { props } from "../src/props";
import { delay } from "../src/delay";
import { expect } from "chai";

describe("props tests", () => {
    context("value check tests", () => {
        const delayMsg = async (output: string, ms: number) => {
            await delay(1000);
            return output;
        };

        const expectedOutput = {
            1: "1",
            2: "2",
            3: "3",
        }

        it("array consist only promises", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(await props({1: promise1, 2: promise2, 3: promise3})).to.eql(expectedOutput);
        });

        it("array consist mix of promises and vals", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(await props({1: promise1, 2: "2", 3:promise3})).to.eql(expectedOutput);
        });

        it("array doesn't consist promises", async () => {
            expect(await props({1: "1", 2: "2", 3: "3"})).to.eql(expectedOutput);
        });
    });
    context("type check", () => {
        it("check if type of 'props' is function", async () => {
            expect(props).to.be.a("function");
        });
    });
});
