import { all } from "../src/all";
import { delay } from "../src/delay";
import { expect } from "chai";

describe("all tests", () => {
    context("value check tests", () => {
        const delayMsg = async (output: string, ms: number) => {
            await delay(1000);
            return output;
        };

        it("array consist only promises", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(await all([promise1, promise2, promise3])).to.eql([
                "1",
                "2",
                "3",
            ]);
        });

        it("array consist mix of promises and vals", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(await all([promise1, "2", promise3])).to.eql([
                "1",
                "2",
                "3",
            ]);
        });

        it("array doesn't consist promises", async () => {
            expect(await all(["1", "2", "3"])).to.eql(["1", "2", "3"]);
        });
    });
    context("type check", () => {
        it("check if type of 'all' is function", async () => {
            expect(all).to.be.a("function");
        });
    });
});
