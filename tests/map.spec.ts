import { mapSeries } from "../src/map";
import { delay } from "../src/delay";
import { expect } from "chai";

describe("map tests", () => {
    context("value check tests", () => {
        const delayMsg = async (output: string, ms: number) => {
            await delay(1000);
            return output;
        };

        const asyncCallbackTest = async (item: any) => {
            await delay(200);
            item = await item;
            return item + item;
        };

        it("array consist only promises", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(
                await mapSeries(
                    [promise1, promise2, promise3],
                    asyncCallbackTest
                )
            ).to.eql(["11", "22", "33"]);
        });

        it("array consist mix of promises and vals", async () => {
            const promise1 = delayMsg("1", 1000);
            const promise3 = delayMsg("3", 1000);

            expect(
                await mapSeries([promise1, "2", promise3], asyncCallbackTest)
            ).to.eql(["11", "22", "33"]);
        });

        it("array doesn't consist promises", async () => {
            expect(await mapSeries(["1", "2", "3"], asyncCallbackTest)).to.eql(["11", "22", "33"]);
        });
    });
    context("type check", () => {
        it("check if type of 'map' is function", async () => {
            expect(mapSeries).to.be.a("function");
        });
    });
});
