import { each } from "../src/each";
import { delay } from "../src/delay";
import { expect } from "chai";

describe("each tests", () => {
    context("value check tests", () => {
        const delayMsg = async (output: string, ms: number) => {
            await delay(1000);
            return output;
        };

        it("array consist only promises", async () => {
            const testArr:string[] = [];
            const promise1 = delayMsg("1", 1000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 1000);
            const asyncCallbackTest = async (item:any) => {
                await delay(200);
                testArr.push(await item);
            }

            await each([promise1,promise2,promise3], asyncCallbackTest);

            expect(testArr).to.eql(["1","2","3"])
        });

        it("array consist mix of promises and vals", async () => {
            const testArr:string[] = [];
            const promise1 = delayMsg("1", 1000);
            const promise3 = delayMsg("3", 1000);
            const asyncCallbackTest = async (item:any) => {
                await delay(200);
                testArr.push(await item);
            }

            await each([promise1,"2",promise3], asyncCallbackTest);

            expect(testArr).to.eql(["1","2","3"])
        });

        it("array doesn't consist promises", async () => {
            const testArr:string[] = [];
            const asyncCallbackTest = async (item:any) => {
                await delay(200);
                testArr.push(await item);
            }

            await each(["1","2","3"], asyncCallbackTest);

            expect(testArr).to.eql(["1","2","3"])
        });
    });
    context("type check", () => {
        it("check if type of 'each' is function", async () => {
            expect(each).to.be.a("function");
        });
    });
});
